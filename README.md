# Simple sudoku solver written in python
(should have so chosen C or any other **proper language** for pass by value instead of fighting this pass by reference gubbins :man: :palm_tree:)


Takes a file path as a parameter, file format is akin to csv

- ```,``` for delimeter
- ```x``` for empty space
- can be presented either as ```\n``` terminated rows or as a single line with rows serialized (That's how positions are stored internally anyway)
- check the 3 example boards
  + 1st is simple solve (no guessing)
  + other two show of recursion solving with guessing
  + minor optimizations are present (guessing from most likely to find)

