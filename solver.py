import sys
import re
import time

class space:

    def __init__(self,value):
        self.value=value
        if self.value == 0:
            self.pos_values=[1,2,3,4,5,6,7,8,9]
        else:
            self.pos_values=[999]


class board:

    def __init__(self, board_file):
        self.board_file=board_file
        self.board_state=self.load_board(board_file)

    def load_board(self, board_file):
        file = open(board_file, 'r')
        content = file.read()
        file.close()
        content = re.sub('\n',',',content)
        content_list= re.split(',',content)
        return_field=[]
        for i in content_list:
            if i == 'x' :
                return_field.append(space(0))
            elif i == '':
                pass
            else:
                return_field.append(space(int(i)))
        return return_field

    def solve(self,field):
        changed = True
        work_field = []
        for k in field:
            work_field.append(space(k.value))
            work_field[len(work_field)-1].pos_values=k.pos_values[:]
            
        while changed:
            changed = False
            for i in range(len(work_field)):
                sole_vals=work_field[i].pos_values
                for y in range(len(work_field)):
                    if work_field[i].value == 0:
                        if ((y % 9 == i % 9) or (y//9 == i//9) or (((i//3)%3 == (y//3)%3) and (y//27 == i//27))) and work_field[y].value !=0 :
                            if work_field[y].value in work_field[i].pos_values:
                                 work_field[i].pos_values.remove(work_field[y].value)
                                 changed = True

                            else:
                                for x in sole_vals:
                                    if x in work_field[y].pos_values:
                                        sole_vals.remove(x)

                if work_field[i].value == 0 and len(work_field[i].pos_values) ==1 :
                    work_field[i].value = work_field[i].pos_values[0]
                    changed = True

                if work_field[i].value == 0 and len(sole_vals) ==1 :
                    work_field[i].value = sole_vals[0]
                    changed = True
        while True:
            gottaGuess = False
            guess = 0
            guess_len= 15
            for x in range(len(work_field)):
                if len(work_field[x].pos_values) == 0 and work_field[x].value == 0:
                    return False

                elif len(work_field[x].pos_values) < guess_len and work_field[x].value == 0:
                    guess = x
                    guess_len = len(work_field[x].pos_values)
                    gottaGuess = True
                    break
            if gottaGuess :
                guess_field = []
                for k in work_field[guess].pos_values:
                    guess_field = []
                    for u in work_field:
                        guess_field.append(space(u.value))
                        guess_field[len(guess_field)-1].pos_values=u.pos_values[:]
                    
                    guess_field[guess].value=k
                    guess_field[guess].pos_values=[999]
                    guessing=self.solve(guess_field)
                    if guessing :
                        return True
            else:
                self.board_state = work_field
                return True
        return False

    def print_board(self,board):
        print ('----'*9)
        for i in range(len(board)):
            if i % 9 == 8 :
                print('',board[i].value ,end=' |\n')
                print ('----'*9)
            else: 
                print('',board[i].value,end=' |')


brd=board(sys.argv[1])
brd.print_board(brd.board_state)
brd.solve( brd.board_state)
brd.print_board(brd.board_state)
